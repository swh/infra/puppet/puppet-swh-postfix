# Define a transport
define postfix::transport (
  String $transport,
  String $destination = $title,
  String $order       = '01',
) {
  concat::fragment {"postfix::transport::${title}":
    target  => '/etc/postfix/transport',
    content => "${destination} ${transport}",
    order   => $order
  }
}
