# Define a virtual alias
define postfix::virtual (
  Variant[String, Array[String]] $destination,
  String                         $alias      = $title,
  String                         $alias_file = '/etc/aliases',
  String                         $order      = '01',
) {

  $domain = $alias.regsubst('^@', '')

  $dest_array = Array($destination, true)

  # Need an intermediate alias if any value starts with | or /
  $needs_intermediate = $dest_array.any |$value| { $value =~ /^[|\/]/ }

  if $needs_intermediate {
    $alias_name = "virtual-for-domain-${domain}"

    concat::fragment {"postfix::virtual::${title}":
      target  => '/etc/postfix/virtual',
      content => "${alias} ${alias_name}",
      order   => $order
    }

    mailalias {$alias_name:
      target    => $alias_file,
      recipient => $dest_array,
    }
  } else {

    $cleaned_recipient = $dest_array.map |$value| {
      $escaped_value = $value.regsubst('(["\\\\])', '\\\\\\1', 'G')
      $value ? {
        /^\|/   => "\"${escaped_value}\"",
        default => $value,
      }
    }.join(', ')

    concat::fragment {"postfix::virtual::${title}":
      target  => '/etc/postfix/virtual',
      content => "${alias} ${cleaned_recipient}",
      order   => $order
    }
  }

  # This one is needed so that
  if $domain != $alias {
    concat::fragment {"postfix::virtual::domain::${title}":
      target  => '/etc/postfix/virtual',
      content => "${domain} ignored",
      order   => $order
    }
  }
}
